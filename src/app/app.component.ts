import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ListPage } from '../pages/list/list';
import { CurrentorderPage } from '../pages/currentorder/currentorder';
import { CardetailsPage } from '../pages/cardetails/cardetails';
import {
    Storage
} from '@ionic/storage';
import {
    Config
} from './config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    public isToggled: boolean;
    availabilityval: any;
    loginstatus: any;
    driverid: any;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public storage: Storage, public events: Events, private http: Http) {
    this.initializeApp();

      
      this.storage.get('UserData').then((datta) => {
          
            console.log(datta);
          if(datta!=null){
              this.driverid=datta.customer_id;
              console.log(datta);
              if(datta.availability==1){
                  this.isToggled = true;
              }
              else{
                  this.isToggled = false;
              }
              this.loginstatus=true;
          }else{
              this.loginstatus=false;
          }
//          if(datta.customer_id!=undefined && datta.customer_id>0){
//              alert('in if');
//          }
//          let userdata=val;
//          console.log(userdata.customer_id);
          if(this.loginstatus==true){
          this.pages = [
      { title: 'Current Order', component: CurrentorderPage },
      { title: 'Car Details', component: CardetailsPage }
    ];
      }
      else{
          this.pages = [
      { title: 'Login', component: LoginPage }
    ];
      }
        });
      
      
      
      
    // used for an example of ngFor and navigation
      events.subscribe('user:login', (user, time) => {
          this.loginstatus=true;
    this.pages = [
        { title: 'Current Order', component: CurrentorderPage },
      { title: 'Car Details', component: CardetailsPage }
    ];
          });
      events.subscribe('user:logout', (time) => {
          this.loginstatus=false;
    this.pages = [
      { title: 'Login', component: LoginPage }
    ];
          });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
logout(event) {
    this.storage.set('UserData', null);
    this.events.publish('user:logout',Date.now());
    
    this.nav.setRoot(LoginPage);
}
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  availability_changer(value){
      if(this.isToggled==false){
          this.availabilityval=0;
      }
      else{
          this.availabilityval=1;
      }
      console.log(this.availabilityval);
      console.log('availability value: '+this.isToggled);
      let url = Config.BASEURL + "change_driver_availability";
        let headers = new Headers();    
        headers.append('Content-Type', 'application/json');

        this.http.post(url, {
            availabilityval: this.availabilityval,
            driverid: this.driverid
        }, {
            headers: headers
            })
            .pipe(map(res => res.json()))
            .subscribe(data => {
            console.log(data);
            this.storage.get('UserData').then((data) => {
                let userdata = {
                    customer_id: data.customer_id,
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    address: data.address,
                    terms_conditions: data.terms_conditions,
                    availability: this.availabilityval,
                    status: data.status,
                    created_date: data.created_date
                };
                this.storage.set('UserData', userdata);
            });
        });
  }
}
