// import { Component } from '@angular/core';
// import {
//     Storage
// } from '@ionic/storage';
// import {
//     App,
//     IonicPage,
//     NavController,
//     NavParams,
//     AlertController,
//     LoadingController,
//     Events
// } from 'ionic-angular';
// import {
//     ForgotpasswordPage
// } from '../forgotpassword/forgotpassword';
// import {
//     RegisterPage
// } from '../register/register';
// import {
//     HomePage
// } from '../home/home';
import {
    CurrentorderPage
} from '../currentorder/currentorder';
// import {
//     Config
// } from '../../app/config.service';



import { RequestOptions, HttpModule } from '@angular/http';

// import 'rxjs/add/operator/map';
// import 'rxjs/Rx';
// import { map } from 'rxjs/operators';
// =============
import { Component } from '@angular/core';
import {
    Storage
} from '@ionic/storage';
import {
    App,
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    Events
} from 'ionic-angular';
import {
    ForgotpasswordPage
} from '../forgotpassword/forgotpassword';
import {
    RegisterPage
} from '../register/register';
import {
    HomePage
} from '../home/home';

import {
    Config
} from '../../app/config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';
import { Headers } from '@angular/http';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    email: any;
    password: any;
    headers: Headers
    options: RequestOptions

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private alertCtrl: AlertController,
        private http: Http,
        public storage: Storage,
        public loadingCtrl: LoadingController,
        public events: Events,
        public appCtrl: App,
    ) {
        this.email = 'wsbill2009@gmail.com'
        this.password = 'Willbabyof78'
        this.storage.get('UserData').then((val) => {
            if (val != null) {
                this.appCtrl.getRootNav().setRoot(CurrentorderPage);
            }
        });
    }

    ionViewDidLoad() {
        this.setUpHeaderAndOptions()
        console.log('ionViewDidLoad LoginPage');
    }
    forgotTapped(event) {
        this.navCtrl.push(ForgotpasswordPage, {
            //      item: item
        });
    }

    registerTapped(event) {
        this.navCtrl.push(RegisterPage, {});
    }
    loginTapped(event) {
        if (this.email == undefined || this.password == undefined) {
            let alertmsg = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Invalid Email or Password!',
                buttons: ['OK']
            });
            alertmsg.present();
        }
        else {
            let loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            let url = Config.BASEURL + "driver_signin";
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');

            this.http.post(url, { email: this.email, password: this.password }, {
                headers: headers
            })
                .pipe(map(res => res.json()))
                .subscribe(data => {
                    console.log(data);
                    loading.dismiss();
                    if (data.response == "invalid") {
                        let alertmsg = this.alertCtrl.create({
                            title: 'Error',
                            subTitle: 'Invalid Email or Password!',
                            buttons: ['OK']
                        });
                        alertmsg.present();
                    } else {
                        let userdata = {
                            customer_id: data.response.id,
                            name: data.response.name,
                            email: data.response.email,
                            phone: data.response.phone,
                            address: data.response.address,
                            terms_conditions: data.response.terms_conditions,
                            availability: data.response.availability,
                            car_make: data.response.car_make,
                            car_model: data.response.car_model,
                            car_year: data.response.car_year,
                            car_color: data.response.car_color,
                            car_number: data.response.car_number,
                            status: data.response.status,
                            created_date: data.response.created_date
                        };
                        this.events.publish('user:login', userdata, Date.now());
                        this.storage.set('UserData', userdata);
                        this.storage.get('UserData').then((val) => {
                            console.log(val);
                            this.appCtrl.getRootNav().setRoot(CurrentorderPage);
                        });
                    }

                }, onError => {
                        loading.dismiss();
                        alert(onError.mess)
                });

        }
    }
    setUpHeaderAndOptions() {
        this.headers = new Headers();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', 'beautyapp.pk');
        this.headers.append('Access-Control-Allow-Methods', 'GET,POST')
        this.headers.append('Access-Control-Allow-Headers', 'Authorization, Content-Type')

        this.options = new RequestOptions({ headers: this.headers });
    }

    getData(params) {
        // const percentEncodedJsonData = encodeURIComponent(JSON.stringify(params))

        let url = Config.BASEURL + "driver_signin";

        return this.http.post(url + params, this.options)
            .map(Response => Response.json());

    }
}
