import { Component } from '@angular/core';
import {
    Storage
} from '@ionic/storage';
import {
    App,
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    Events,
    Platform
} from 'ionic-angular';
import {
    Config
} from '../../app/config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers } from '@angular/http';

import {
    LoginPage
} from '../login/login';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { map } from 'rxjs/operators';
/**
 * Generated class for the CurrentorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-currentorder',
    templateUrl: 'currentorder.html',
})
export class CurrentorderPage {
    driverid: any;
    baseurl: any;
    order_address_pickup: any;
    order_address_destination: any;
    totaldistance: any;
    order_date: any;
    order_time: any;
    totalprice: any;
    pet_name: any;
    animal_type: any;
    pet_size: any;
    biting_history: any;
    physical_condition: any;
    older_then_six: any;
    medication: any;
    pet_image: any;
    order_status: any;
    orderid: any;


    constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private http: Http, public storage: Storage, public loadingCtrl: LoadingController, public events: Events, public appCtrl: App, private geolocation: Geolocation,
        public platform: Platform,
    ) {
        this.storage.get('UserData').then((val) => {
            if (val != null) {
                this.driverid = val.customer_id;
                this.get_driver_currentorder();
            }
        });
        platform.ready().then(()=>  {

            this.start_gps_tracking();
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CurrentorderPage');
    }
    get_driver_currentorder() {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        let url = Config.BASEURL + "get_driver_currentorder";
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(url, {
            driverid: this.driverid
        }, {
                headers: headers
            })
            .pipe(map(res => res.json()))
            .subscribe(data => {
                loading.dismiss();
                if (data.response == "N/A") {

                } else {
                    this.baseurl = Config.IMG_BASEURL;
                    this.order_address_pickup = data.response.order_address_pickup;
                    this.order_address_destination = data.response.order_address_destination;
                    this.totaldistance = data.response.distance;
                    this.order_date = data.response.order_date;
                    this.order_time = data.response.order_time;
                    //                this.totalprice=data.response.order_total;
                    this.totalprice = (8 + (0.49 * this.totaldistance)).toFixed(2);
                    this.pet_name = data.response.pet_name;
                    this.animal_type = data.response.animal_type;
                    this.pet_size = data.response.pet_size;
                    this.biting_history = data.response.biting_history;
                    this.physical_condition = data.response.physical_condition;
                    this.older_then_six = data.response.older_then_six;
                    this.medication = data.response.medication;
                    this.pet_image = data.response.pet_image;
                    this.order_status = data.response.status;
                    this.orderid = data.response.id;
                }
                console.log(data);
            }, onError => {
                loading.dismiss();
                alert(onError.mess)
        });
    }
    pickedorder(event) {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        let url = Config.BASEURL + "order_picked";
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(url, {
            orderid: this.orderid
        }, {
                headers: headers
            })
            .pipe(map(res => res.json()))
            .subscribe(data => {
                loading.dismiss();
                if (data.response == "success") {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Thanks',
                        subTitle: 'You have successfully changes the order status.',
                        buttons: [
                            {
                                text: 'Ok',
                                handler: () => {
                                    this.appCtrl.getRootNav().setRoot(LoginPage);
                                }
                            }
                        ]
                    });
                    alertmsg.present();
                } else {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'Something went wrong! Please try again later.',
                        buttons: ['OK']
                    });
                    alertmsg.present();
                }
                console.log(data);
            }, onError => {
                loading.dismiss();
                alert(onError.mess)
        });
    }
    order_delivered(event) {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        let url = Config.BASEURL + "order_delivered";
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(url, {
            orderid: this.orderid
        }, {
                headers: headers
            })
            .pipe(map(res => res.json()))
            .subscribe(data => {
                loading.dismiss();
                if (data.response == "success") {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Thanks',
                        subTitle: 'You have successfully changes the order status.',
                        buttons: [
                            {
                                text: 'Ok',
                                handler: () => {
                                    this.appCtrl.getRootNav().setRoot(LoginPage);
                                }
                            }
                        ]
                    });
                    alertmsg.present();
                } else {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'Something went wrong! Please try again later.',
                        buttons: ['OK']
                    });
                    alertmsg.present();
                }
                console.log(data);
            }, onError => {
                loading.dismiss();
                alert(onError.mess)
        });
    }

    start_gps_tracking() {
        var watchOptions = {
            timeout: 5000,
            maxAge: 0,
            enableHighAccuracy: true
        };
        //     let watch = this.geolocation.watchPosition();
        //watch.subscribe((data) => {
        ////    alert('in positions');
        //    var latitude=data.coords.latitude;
        //    var longitude=data.coords.longitude;
        // // data can be a set of coordinates, or an error (if an error occurred).
        // // data.coords.latitude
        // // data.coords.longitude
        //    
        //    let url = Config.BASEURL + "update_driver_location";
        //        let headers = new Headers();    
        //        headers.append('Content-Type', 'application/json');
        //
        //        this.http.post(url, {
        //            driverid: this.driverid,
        //            latitude: latitude,
        //            longitude: longitude
        //        }, {
        //            headers: headers
        //        }).map(res => res.json()).subscribe(data => {
        //            console.log(data);
        //        });
        //    
        //    
        //    
        //});

        var options = {
            timeout: 5000,
            maximumAge: 0,
            enableHighAccuracy: true
        };

        try {
            
            this.geolocation.getCurrentPosition().then(location => {
                console.log(location);
    
            })
        } catch (error) {
            
        }

        //     .then((resp) => {



        //     //        alert(JSON.stringify(resp.coords.latitude));

        //     //        alert(JSON.stringify(resp.coords.longitude));

        //     console.log('location');

        //     console.log(resp);

        // }, (error) => {

        //     //    alert('in error');

        //     //    alert(error.PositionError);

        // }).catch((err) => {

        //     //    alert('in catch');

        //     //    alert(JSON.stringify(err));

        // });

    }

    onSuccess = function (position) {

        alert('Latitude: ' + position.coords.latitude + '\n' +

            'Longitude: ' + position.coords.longitude + '\n' +

            'Altitude: ' + position.coords.altitude + '\n' +

            'Accuracy: ' + position.coords.accuracy + '\n' +

            'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +

            'Heading: ' + position.coords.heading + '\n' +

            'Speed: ' + position.coords.speed + '\n' +

            'Timestamp: ' + position.timestamp + '\n');

    };



    // onError Callback receives a PositionError object

    //

    onError(error) {

        alert('code: ' + error.code + '\n' +

            'message: ' + error.message + '\n');

    }
}
