import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    LoadingController
} from 'ionic-angular';

import {
    LoginPage
} from '../login/login';
import {
    Config
} from '../../app/config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers } from '@angular/http';
/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-forgotpassword',
    templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
    email: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private http: Http, public loadingCtrl: LoadingController) {

    }

    forgotTapped(event) {
        if (this.email == undefined) {
            let alertmsg = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please enter a proper email address.',
                buttons: ['OK']
            });
            alertmsg.present();
        } else {
            let loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            let url = Config.BASEURL + "forgot_password_driver";
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');

            this.http.post(url, {
                email: this.email
            }, {
                headers: headers
            }).map(res => res.json()).subscribe(data => {
                console.log(data);
                loading.dismiss();
                if (data.response == "invalidemail") {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'This email does not exist.',
                        buttons: ['OK']
                    });
                    alertmsg.present();
                } else if (data.response == "success") {
                    let alertmsg = this.alertCtrl.create({
                        title: 'Success',
                        message: 'New password is updated successfully. Please check your email for latest password.',
                        buttons: [
                            {
                                text: 'OK',
                                role: 'ok',
                                handler: () => {
                                    this.navCtrl.push(LoginPage, {});
                                }
      }
    ]
                    });
                    alertmsg.present();
                }
                else{
                    let alertmsg = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'Something went wrong please try again later.',
                        buttons: ['OK']
                    });
                    alertmsg.present();
                }

            }, onError => {
                loading.dismiss();
                alert(onError.mess)
        });
        }
    }

}
