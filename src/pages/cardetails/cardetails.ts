import {
    Component
} from '@angular/core';
import {
    Storage
} from '@ionic/storage';
import {
    App,
    ViewController,
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    ToastController 
} from 'ionic-angular';
import {
    Config
} from '../../app/config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import {
    Validators,
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';
import {
    LoginPage
} from '../login/login';

import { Headers } from '@angular/http';

/**
 * Generated class for the CardetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cardetails',
  templateUrl: 'cardetails.html',
})
export class CardetailsPage {
validations_form: FormGroup;
car_make: any;
car_model: any;
car_year: any;
car_color: any;
car_number: any;
driverid: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private alertCtrl: AlertController, private http: Http, public storage: Storage, public loadingCtrl: LoadingController, public appCtrl: App, public viewCtrl: ViewController, private toastCtrl: ToastController) {
       this.storage.get('UserData').then((val) => {
            if(val==null){
                this.appCtrl.getRootNav().setRoot(LoginPage);
            }
           else{
               this.car_make=val.car_make;
               this.car_model=val.car_model;
               this.car_year=val.car_year;
               this.car_color=val.car_color;
               this.car_number=val.car_number;
               this.driverid=val.customer_id;
               console.log(val);
           }
        });
  }

  ionViewWillLoad() {
    
        this.validations_form = this.formBuilder.group({
            car_make: new FormControl('', Validators.compose([
        Validators.required
      ])),
            car_model: new FormControl('', Validators.compose([
        Validators.required,
      ])),
            car_year: new FormControl('', Validators.compose([
        Validators.required
      ])),
            car_color: new FormControl('', Validators.compose([
        Validators.required
      ])),
            car_number: new FormControl('', Validators.compose([
        Validators.required,
      ])),
            terms: false,
        });
    }

    validation_messages = {
        'car_make': [
            {
                type: 'required',
                message: 'Car Make is required.'
            }
    ],
        'car_model': [
            {
                type: 'required',
                message: 'Car Model is required.'
            }
    ],
        'car_year': [
            {
                type: 'required',
                message: 'Car year is required.'
            }
    ],
        'car_color': [
            {
                type: 'required',
                message: 'Car color is required.'
            }
    ],
        'car_number': [
            {
                type: 'required',
                message: 'Car number is required.'
            }
    ],
    };

oncardetailSubmit(posted_values){
    let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
    let url = Config.BASEURL + "update_car_details";
        let headers = new Headers();    
        headers.append('Content-Type', 'application/json');

        this.http.post(url, {
            driverid: this.driverid,
            car_make: posted_values.car_make,
            car_model: posted_values.car_model,
            car_year: posted_values.car_year,
            car_color: posted_values.car_color,
            car_number: posted_values.car_number
        }, {
            headers: headers
        }).map(res => res.json()).subscribe(data => {
            console.log(data);
            loading.dismiss();
            if(data.response == "success"){
                this.storage.get('UserData').then((val) => {
                    if(val!=null){
                        let userdata = {
                    customer_id: val.customer_id,
                    name: val.name,
                    email: val.email,
                    phone: val.phone,
                    address: val.address,
                    terms_conditions: val.terms_conditions,
                    availability: val.availability,
                    car_make: posted_values.car_make,
                    car_model: posted_values.car_model,
                    car_year: posted_values.car_year,
                    car_color: posted_values.car_color,
                    car_number: posted_values.car_number,
                    status: val.status,
                    created_date: val.created_date
                };
                this.storage.set('UserData', userdata);
                    }
           
        });
                let alertmsg = this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Car information is updated successfully!',
                    buttons: ['OK']
                });
                alertmsg.present();
            }
            else{
                let alertmsg = this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something went wrong please try again later!',
                    buttons: ['OK']
                });
                alertmsg.present();
            }

        }, onError => {
            loading.dismiss();
            alert(onError.mess)
    });
}

}
