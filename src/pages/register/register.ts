import {
    Component
} from '@angular/core';
import {
    Storage
} from '@ionic/storage';
import {
    App,
    ViewController,
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    Config
} from '../../app/config.service';
import {
    Http
} from '@angular/http';
import 'rxjs/add/operator/map';
import {
    Validators,
    FormBuilder,
    FormGroup,
    FormControl
} from '@angular/forms';
import {
    LoginPage
} from '../login/login';
import {
    TermsPage
} from '../terms/terms';
import { Headers } from '@angular/http';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {
    validations_form: FormGroup;
    selectedItem: any;
    driver_image: any;
    imageURI: any;
    icons: string[];
    items: Array<{
        title: string,
        note: string,
        icon: string
    }>;

    constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private alertCtrl: AlertController, private http: Http, public storage: Storage, public loadingCtrl: LoadingController, public appCtrl: App, public viewCtrl: ViewController, private toastCtrl: ToastController, private camera: Camera, private transfer: FileTransfer) {

    }
    ionViewWillLoad() {

        this.validations_form = this.formBuilder.group({
            name: new FormControl('', Validators.compose([
                Validators.minLength(3),
                Validators.required
            ])),
            mobile: new FormControl('', Validators.compose([
                Validators.required,
            ])),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.minLength(6),
                Validators.required
            ])),
            address: new FormControl('', Validators.compose([
                Validators.required,
            ])), postcode: new FormControl('', Validators.compose([
                Validators.required,
            ])), state: new FormControl('', Validators.compose([
                Validators.required,
            ])),
            terms: false,
        });
    }

    validation_messages = {
        'name': [
            {
                type: 'required',
                message: 'Name is required.'
            },
            {
                type: 'minlength',
                message: 'Name must be at least 3 characters long.'
            }
        ],
        'mobile': [
            {
                type: 'required',
                message: 'Phone Number is required.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Email is required.'
            },
            {
                type: 'pattern',
                message: 'Enter a valid email.'
            }
        ],
        'password': [
            {
                type: 'required',
                message: 'Password is required.'
            },
            {
                type: 'minlength',
                message: 'Password must be at least 6 characters long.'
            }
        ],
        'address': [
            {
                type: 'required',
                message: 'Address is required.'
            }
        ], 'postcode': [
            {
                type: 'required',
                message: 'Postcode is required.'
            }
        ], 'state': [
            {
                type: 'required',
                message: 'State is required.'
            }
        ],
    };
    onRegisterSubmit(posted_values) {

        //        console.log(posted_values);
        if (posted_values.terms == false) {
            let toast = this.toastCtrl.create({
                message: 'Please agree with the terms and conditions',
                duration: 3000,
                position: 'bottom',
                showCloseButton: true,
                closeButtonText: "Ok"
            });
            toast.present();
        }
        //        else if(this.imageURI==undefined){
        //        let alertmsg = this.alertCtrl.create({
        //                    title: 'Error',
        //                    subTitle: 'Please add your image.',
        //                    buttons: ['OK']
        //                });
        //                alertmsg.present();
        //    }
        else {
            //            let loading = this.loadingCtrl.create({
            //            content: 'Please wait...'
            //        });
            //        loading.present();



            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'ionicfile',
                fileName: 'ionicfile',
                chunkedMode: false,
                mimeType: "image/jpeg",
                headers: {}
            }
            fileTransfer.upload(this.imageURI, Config.BASEURL + "uploaddriverimage", options)
                .then((data) => {
                    this.driver_image = data.response;
                    //        alert(this.driver_image);

                    let url = Config.BASEURL + "signup_driver";
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');

                    this.http.post(url, {
                        name: posted_values.name,
                        email: posted_values.email,
                        password: posted_values.password,
                        address: posted_values.address,
                        postcode: posted_values.postcode,
                        state: posted_values.state,
                        phone: posted_values.mobile,
                        image: this.driver_image
                    }, {
                            headers: headers
                        }).map(res => res.json()).subscribe(data => {
                            //            alert('in second');
                            //            loading.dismiss();
                            console.log(data);
                            if (data.response == "sameemail") {
                                let alertmsg = this.alertCtrl.create({
                                    title: 'Error',
                                    subTitle: 'Driver with same  email already exists!',
                                    buttons: ['OK']
                                });
                                alertmsg.present();
                            }
                            else if (data.response == "success") {
                                let alertmsg = this.alertCtrl.create({
                                    title: 'Success',
                                    subTitle: 'Your account has been created successfully!',
                                    buttons: ['OK']
                                });
                                alertmsg.present();
                                this.navCtrl.setRoot(LoginPage);
                            }
                            else {
                                let alertmsg = this.alertCtrl.create({
                                    title: 'Error',
                                    subTitle: 'Something went wrong please try again later!',
                                    buttons: ['OK']
                                });
                                alertmsg.present();
                            }

                        });
                }, (err) => {
                    alert('error');
                });

        }
    }
    getImage() {
        alert('came to get picture')
        // add this param in option to get picture from photo library (,sourceType: this.camera.PictureSourceType.PHOTOLIBRARY)
        const ooptions: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        try {
            
            this.camera.getPicture(ooptions).then((imageData) => {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64 (DATA_URL):
                alert('dsfasda')
                let base64Image = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {
                alert(JSON.stringify(err));
                console.log(err);
                }).catch(er => {
                    console.log(er);
            });
        } catch (error) {
            console.log(error);
            
            // alert(JSON.stringify(error.code))
            // alert(JSON.stringify(error.message))
            alert(JSON.stringify(error))
        }
    // return 
        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        }

        this.camera.getPicture(options).then((imageData) => {
            //      this.imageFileName="data:image/jpeg;base64," + imageData;
            this.imageURI = imageData;

        }, (err) => {
            console.log(err);
        });
    }
    termsTapped(event) {
        this.navCtrl.push(TermsPage, {});
    }

    captureImageFromCamera() {
        var options = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.CAMERA,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
        };
        this.camera.getPicture(options).then((imagePath) => {
            
            this.imageURI = imagePath

        }, (err) => {
            console.log(err);
        });
    }
}
